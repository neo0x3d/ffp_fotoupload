# Udev

Use UDEV to start upload when device is plugged in.

Caution: This will lead to SD cards being automatically and instantly processed on this specific device. May not be suitable if people might want to use the SD card reader for other purposes.

## Setup

1. Obtain the USB Vendor ID and Product ID from the target SD card reader. (only cards which are plugged into this reader will start the upload)
2. Add the USB VID and PID to the 99-ffp_multiupload.rules file
3. Copy the udev rules file to the rules.d directory and reload udev rules

  ```
  # cp ./99-ffp_multiupload.rules /etc/udev/rules.d/
  # udevadm control --reload-rules
  ```
To confirm, insert SD card reader, then SD card confirm that upon SD card being plugged in the device /dev/fotoupload is generated.
