# Arduino Setup

Use an physical button connected to an Arduino to trigger an upload.

## Process description

1. user presses physical button
2. Arduino detects button press, sends HID keyboard commands to computer over USB
3. computer assigns HID commands to keyboard shortcut and executes specified command

## Required Hardware

1. Arduino Pro Micro or Leonardo (needs Atmega 32U4 microcontroller)
2. normally open button + wiring
3. USB adapter cable for the Arduino
4. enclosure to house everything

## Assembly

1. solder two leads to the button, insulate the solder joints with heat shrink tube
2. terminate the leads with a female 2.54mm header, insulate solder joints with heat shrink tube
3. solder a 2.54mm header to pin 2 and GND on the Arduino Pro Micro
4. mount the button in the front of an enclosure
5. connect the cable from the button to the header on the Arduino (Pin 2 and GND)
6. connect the Arduino via an USB adapter cable to the computer

## Optional: filter for SD card (reader)

This optional step will ensure only SD cards inserted into a specific reader will be processed upon button press. To achive this, an udev rule is used, which filters for a USB device (via VID/PID) and generates a symlink for the device under /dev/fotoupload. This symlink will used in the keyboard shortcut.

The same udev file from [bootstrap/udev_autorun](/boostrap/udev_autorun/UDEV.md) is used with modification:

1. to prevent automatic processing, comment out the line: "ACTION=="add", RUN+="/usr/bin/curl --data 'upload=%E{DEVNAME}' localhost:9310""
2. set up the udev rule as shown in [bootstrap/udev_autorun](/boostrap/udev_autorun/UDEV.md)

## Keyboard Shortcut (user needs to be logged in)

The keyboard shortcut in the arduino sketch is set to: Crtl + Alt + u

1. find out under which name under /dev/ the SD card shows up on the computer, e.g. /dev/mmcblk0
2. add a new Application Shortcut on your host computer (CentOS 7) under Keyboard -> Application Shortcuts
3. set the command to (with the correct device name): curl -d "upload=/dev/mmcblk0" 127.0.0.1:9310
4. manually press Ctrl + Alt + u

## Programming the Arduino

1. obtain the Arduino IDE from <https://www.arduino.cc/en/Main/Software>
2. add support for the Pro Micro via the board manager: <https://learn.sparkfun.com/tutorials/pro-micro--fio-v3-hookup-guide#installing-mac--linux>
3. open the Arduino IDE and load the arduino_button.ino sketch
4. select the appropriate board in the Arduino IDE under Tools -> board and the port under Tools -> Port (will show up after an Arduino has been detected)
5. bridge the reset jumper on the Arduino to trigger the bootloader, upload compiled sketch from the IDE
