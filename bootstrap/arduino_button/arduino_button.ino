#include <Keyboard.h>
/*
File: arduino_bootstap.ino
Author: Marius Pfeffer / neo0x3d
Description: Send ctrl + m as HID commands via USB

Hardware:
* Arduino Pro Micro (ATMega32u4)
* User button (normally open) connected between Pin 2 and GND
* Arduino connected via USB to computer
*/

int button = 2; //trigger on pin 2
int timeout_miliseconds = 2500; //timout of 2.5 seconds to prevent multiple starts
int pressed = 0;

void setup() {
    pinMode(button, INPUT_PULLUP); //setup internal pullup
    Keyboard.begin();
}

void loop() {
    pressed = digitalRead(button); //manually poll the pin
    if (pressed == 0)
    {
      while(pressed == 0) // crusty way to make sure it only triggers on one button press
      {
        pressed = digitalRead(button);
      }
        Keyboard.press(KEY_ESC);
        delay(50);
        Keyboard.releaseAll();
        delay(50);
        Keyboard.press(KEY_LEFT_CTRL);
        Keyboard.press(KEY_LEFT_ALT);
        Keyboard.press('u');
        delay(50);
        Keyboard.releaseAll();

        delay(timeout_miliseconds); // ignore all button presses within the timeout
    }
    delay(25);
}
