# SystemD Daemon

Systemd unit for the main python script. (start the script as daemon and keep alive)

## Setup

1. Edit the path in the ffp_multiupload@.service file pointing to the main Python script
2. Copy Systemd unit template file to the systemds system folder
3. Enable and start the Systemd unit with the selected user

```
# cp ./ffp_multiupload.service  /etc/systemd/system/
# systemctl enable ffp_multiupload@user.service
# systemctl start ffp_multiupload@user.service
```
