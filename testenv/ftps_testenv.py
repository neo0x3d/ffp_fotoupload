#!/usr/bin/env python3
import os

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import TLS_FTPHandler
from pyftpdlib.servers import FTPServer

from threading import Thread


CERTFILE = os.path.abspath(os.path.join(
    os.path.dirname(__file__), "keycert.pem"))

FTP_IP = '127.0.0.1'
FTP_PORT = 5555


def spinup_ftpserver():
    authorizer = DummyAuthorizer()
    authorizer.add_user('localFTPUser', 'localFTPPass',
                        './localFTP', perm='elradfmw')
    authorizer.add_user('remoteFTPUser', 'remoteFTPPass',
                        './remoteFTP', perm='elradfmw')
    # authorizer.add_anonymous('.')
    handler = TLS_FTPHandler
    handler.certfile = CERTFILE
    handler.authorizer = authorizer
    # requires SSL for both control and data channel
    handler.tls_control_required = True
    handler.tls_data_required = True
    server = FTPServer((FTP_IP, FTP_PORT), handler)
    server.serve_forever()


def gen_ftp_user_dir():
    """
        Generate the (root?) dir for both ftp users, will be used by the ftp server
    """
    LOCALFTP_PATH = os.path.join(os.path.abspath(os.path.dirname(
        __file__)), "localFTP", "datenaustausch/fotoupload")
    REMOTEFTP_PATH = os.path.join(os.path.abspath(os.path.dirname(
        __file__)), "remoteFTP", "public_html/site/images/stories/upload")
    if not os.path.exists(LOCALFTP_PATH):
        os.makedirs(LOCALFTP_PATH)
    if not os.path.exists(REMOTEFTP_PATH):
        os.makedirs(REMOTEFTP_PATH)


if __name__ == '__main__':
    gen_ftp_user_dir()
    spinup_ftpserver()
