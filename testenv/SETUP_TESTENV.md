# Set up ftp server with two users for testing


In order to work with ssl we need to generate keycert.pem, remember the password when generating as it will be needed later.

```
$ openssl req -x509 -newkey rsa:2048 -keyout keycert.pem -out keycert.pem -days 1000
```

# Start test environment


```
$ ./ftps_testenv.py
```
